docker run -it --rm \
  -v "$(pwd)"/data:/app/data:ro \
  -v "$(pwd)"/watermark.png:/app/watermark.png:ro \
  -v "$(pwd)"/cache:/app/cache \
  -v "$(pwd)"/config.yaml:/app/config.yaml \
  --net=host \
  mon7/img-hall-be "$@"
