FROM haskell:9.2.1-buster AS builder
WORKDIR /app

RUN apt-get -y update
RUN apt-get -y install libgmp-dev curl build-essential netbase tree xxd
RUN mkdir -p ~/.local/bin
RUN export PATH=$HOME/.local/bin:$PATH
RUN curl --location https://get.haskellstack.org/stable/linux-x86_64.tar.gz | tar xz --wildcards --strip-components=1 --directory ~/.local/bin '*/stack'

COPY stack.yaml .
COPY package.yaml .
RUN stack setup
RUN stack build --only-dependencies

COPY . .

RUN stack build
RUN stack test
RUN stack install

RUN stack exec img-hall-be-exe -- --version

# runner

FROM haskell:9.2.1-buster
WORKDIR /app

COPY --from=builder /root/.local/bin/img-hall-be-exe /app
RUN /app/img-hall-be-exe --version

ENV PATH=$PATH:/app

ENTRYPOINT ["/app/img-hall-be-exe"]
