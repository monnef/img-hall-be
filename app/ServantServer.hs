{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeOperators #-}

module ServantServer
  ( runServantServer,
  )
where

import Control.Applicative ((<|>))
import Control.Concurrent.STM.TVar
  ( TVar,
    newTVar,
    readTVar,
    writeTVar,
  )
import Control.Lens
import Control.Monad.Except
import Control.Monad.Reader (ReaderT, ask, runReaderT)
import Control.Monad.STM (atomically)
import Data
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Lazy.Char8 as CL
import qualified Data.HashMap.Lazy as M
import Data.Streaming.Network.Internal (HostPreference (Host))
import Data.Text (Text)
import qualified Data.Text as T
import Network.Wai
import Network.Wai.Handler.Warp
import Servant
import Utils

type AppM = ReaderT ServerState Handler

type API1 =
  "categories" :> Get '[JSON] [CategoryInfo] --
    :<|> "categories" :> Capture "id" Text :> Get '[JSON] CategoryDetail --
    :<|> "images" :> Capture "id" Text :> Get '[OctetStream] CL.ByteString --

getAllCats :: AppM [FullCategory]
getAllCats = ask <&> view categories >>= liftIO . atomically . readTVar

getIdToCat :: AppM IdToCat
getIdToCat = ask <&> view idToCat >>= liftIO . atomically . readTVar

getIdToImage :: AppM IdToImage
getIdToImage = ask <&> view idToImage >>= liftIO . atomically . readTVar

getIdToImageThumb :: AppM IdToImage
getIdToImageThumb = ask <&> view idToImageThumbnail >>= liftIO . atomically . readTVar

server1 :: ServerT API1 AppM
server1 = getCategoriesE :<|> getCategoryE :<|> getImageE
  where
    getCategoriesE :: AppM [CategoryInfo]
    getCategoriesE =
      -- TODO: access level
      do
        cats <- getAllCats
        return $ cats <&> (^. info)
    getCategoryE :: Text -> AppM CategoryDetail
    getCategoryE catId =
      -- TODO: access level
      do
        idToCat <- getIdToCat
        idToCat & findCat catId <&> view detail & or404
    getImageE :: Text -> AppM CL.ByteString
    getImageE imgId =
      -- TODO: access level
      do
        idToImg <- getIdToImage
        idToImgThumb <- getIdToImageThumb
        let foundImg = M.lookup (ImageId imgId) idToImg <&> \i -> (i, i ^. path)
        let foundThumb = M.lookup (ImageId imgId) idToImgThumb <&> \i -> (i, i ^. thumbnailPath)
        (img, filePath) <- foundImg <|> foundThumb & or404
        let isThumbnail = filePath == img ^. thumbnailPath
        liftIO $ BL.readFile filePath
    or404 :: Maybe a -> AppM a
    or404 (Just x) = return x
    or404 Nothing = throwError err404

api1 :: Proxy API1
api1 = Proxy

nt :: ServerState -> AppM a -> Handler a
nt s x = runReaderT x s

app1 :: ServerState -> Application
app1 s = serve api1 $ hoistServer api1 (nt s) server1

runServantServer :: ServerRunner
runServantServer initState onBeforeServerStart hostStr port corsOriginMay = do
  let app = app1 {-removeServerHeaderMiddleware-}
  let settings =
        defaultSettings & setPort port & setServerName "" & setBeforeMainLoop onBeforeServerStart
          & setHost (hostStr & T.unpack & Host)
  runSettings settings $ app initState
