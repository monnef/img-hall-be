{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module CmdArgsParser where

import           Data
import           Utils

import           Data.Text                     (Text)
import qualified Data.Text                     as T
import           Options.Applicative
import qualified Options.Applicative           as OA
import           Text.InterpolatedString.Perl6 (qq)

textReader :: ReadM Text
textReader = maybeReader $ T.pack >>> Just

cmdArgsParserInfo :: ParserInfo CmdArgs
cmdArgsParserInfo = OA.info (cmdArgsParser <**> helper) (fullDesc <> progDesc "")

defaultConfigFileName :: Text
defaultConfigFileName = "config.yaml"

cmdArgsParser :: Parser CmdArgs
cmdArgsParser =
  CmdArgs <$> --
  switch (long "generate-config" <> genConfigHelp) <*>
  strOption (long "config" <> short 'c' <> value defaultConfigFileName <> metavar "FILENAME" <> configHelp) <*>
  switch (long "version")
  where
    genConfigHelp = help [qq|Writes default configuration to $defaultConfigFileName|]
    configHelp = help [qq|Set configuration file name (default: $defaultConfigFileName)|]

parseCmdArgs :: IO CmdArgs
parseCmdArgs = execParser cmdArgsParserInfo
