{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

module YesodServer
  ( runYesodServer,
  )
where

import Control.Applicative ((<|>))
import Control.Concurrent.STM.TVar
  ( TVar,
    readTVar,
  )
import Control.Lens
import Control.Monad.Except
import Control.Monad.STM (atomically)
import Data
import qualified Data.ByteString as BS
import qualified Data.HashMap.Lazy as M
import Data.Maybe (fromMaybe)
import Data.Streaming.Network.Internal (HostPreference (Host))
import Data.Text (Text)
import qualified Data.Text as T
import GHC.Generics (Generic)
import Network.Wai (Middleware)
import Network.Wai.Handler.Warp
  ( defaultSettings,
    runSettings,
    setBeforeMainLoop,
    setHost,
    setPort,
    setServerName,
  )
import Network.Wai.Middleware.AddHeaders (addHeaders)
import Utils
import Yesod

fakeViewPatternToStopFormatterFromRemovingLanguageExtension ((== 1) -> True) = undefined

data App = App
  { _state :: ServerState
  }
  deriving (Eq, Generic)

makeFieldsNoPrefix ''App

mkYesod
  "App"
  [parseRoutes|
/categories CategoriesR GET
/categories/#Text CategoryR GET
/images/#Text ImageR GET
|]

--instance Yesod App
instance Yesod App where
  makeSessionBackend _ = return Nothing

zoomState :: (ServerState -> TVar a) -> Handler a
zoomState f = getYesod <&> (view state >>> f) >>= liftIO . atomically . readTVar

or404 :: ToJSON a => Maybe a -> Handler Value
or404 Nothing = notFound
or404 (Just x) = returnJson x

or404raw :: Maybe a -> Handler a
or404raw Nothing = notFound
or404raw (Just x) = return x

getCategoriesR :: Handler Value
getCategoriesR = do
  cats <- zoomState (^. categories)
  returnJson $ cats <&> view info

getCategoryR :: Text -> Handler Value
getCategoryR catId = do
  idToCat <- zoomState (^. idToCat)
  idToCat & findCat catId <&> view detail & or404

getImageR :: Text -> Handler TypedContent
getImageR imgId = do
  idToImg <- zoomState (^. idToImage)
  idToImgThumb <- zoomState (^. idToImageThumbnail)
  let foundImg = M.lookup (ImageId imgId) idToImg <&> \i -> (i, i ^. path)
  let foundThumb = M.lookup (ImageId imgId) idToImgThumb <&> \i -> (i, i ^. thumbnailPath)
  (img, filePath) <- foundImg <|> foundThumb & or404raw
  let isThumbnail = filePath == img ^. thumbnailPath
  imgData <- liftIO $ BS.readFile filePath
  return $ TypedContent typeOctet $ toContent imgData

-- TODO: gzip
runYesodServer :: ServerRunner
runYesodServer initState onBeforeServerStart hostStr port corsOriginMay = do
  waiApp <- toWaiApp App {_state = initState}
  let settings =
        defaultSettings & setPort port & setServerName "" & setBeforeMainLoop onBeforeServerStart
          & setHost (hostStr & T.unpack & Host)
  let corsMiddleware :: Middleware = addHeaders [("Access-Control-Allow-Origin", fromMaybe "*" corsOriginMay & toS)]
  waiApp & corsMiddleware & runSettings settings
