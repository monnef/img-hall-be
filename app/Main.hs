{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import BuildInfo (versionText)
import CmdArgsParser
import Control.Applicative ((<|>))
import Control.Concurrent.STM.TVar
  ( TVar,
    newTVar,
    readTVar,
    writeTVar,
  )
import Control.Exception
  ( Exception,
    SomeException,
    displayException,
    try,
  )
import Control.Lens
import Control.Monad.Except
import Control.Monad.STM (atomically)
import Crawler
import Data
import Data.Aeson
import Data.Bool (bool)
import qualified Data.HashMap.Lazy as M
import Data.List (sortOn)
import Data.Maybe (fromMaybe)
import qualified Data.Text as T
import Data.Tuple.Extra (fst3, snd3, thd3)
import qualified Data.Yaml as Y
import GHC.Exts (sortWith)
import System.Exit (exitSuccess)
import System.IO
  ( hFlush,
    hPutStrLn,
    stderr,
    stdout,
  )
import Text.InterpolatedString.Perl6 (qq)
import Text.Show.Pretty (ppShow)
import Utils
import YesodServer
import Prelude hiding (id)

printOkMessage :: MonadIO m => m ()
printOkMessage = liftIO $ putStrLn " ... OK"

printActionMessage :: MonadIO m => Text -> m ()
printActionMessage msg = liftIO (putStr $ logPrefix <> msg <> " ... " & T.unpack) >> liftIO (hFlush stdout)

logPrefix :: Text
logPrefix = "[img-hall-be] "

okWrapper :: MonadIO m => Text -> m a -> m a
okWrapper msg action = do
  printActionMessage msg
  res <- action
  printOkMessage
  return res

fullImageToDetail :: FullImage -> ImageDetail
fullImageToDetail FullImage {..} = ImageDetail {..}

partialCategoryToFull :: PartialCategory -> FullCategory
partialCategoryToFull c@PartialCategory {..} = FullCategory {_images = images, _subcategories = subcategories, ..}
  where
    images = fromMaybe [] _images
    _detail = partialToDetail c
    _info = partialToInfo c
    subcategories = _subcategories <&> fst <&> partialCategoryToFull
    partialToDetail PartialCategory {..} =
      CategoryDetail
        { _subcategories =
            _subcategories <&> fst <&> partialToDetail
              & sortWith (\x -> x ^. label & unpackTranslatedText & M.lookupDefault "" EN),
          _images = fromMaybe [] _images <&> fullImageToDetail & sortOn (^. date) & reverse,
          ..
        }
    partialToInfo PartialCategory {..} = CategoryInfo {_subcategories = _subcategories <&> fst <&> partialToInfo, ..}

debugFormatCats :: [FullCategory] -> Text
debugFormatCats cats = cats <&> formatCat & T.intercalate "\n"
  where
    formatCat c = ppShow c & T.pack

type ExtractMapsRes = ([(CategoryId, FullCategory)], [(ImageId, FullImage)], [(ImageId, FullImage)])

extractMaps :: [FullCategory] -> ExtractMapsRes
extractMaps cats = cats <&> ex & foldl j ([], [], [])
  where
    ex :: FullCategory -> ExtractMapsRes
    ex c@FullCategory {..} =
      let subRs = _subcategories <&> ex
          mapSubRs f = subRs <&> f & concat
       in ( [(_id, c)] <> mapSubRs fst3,
            (_images <&> (\i -> (i ^. id, i))) <> mapSubRs snd3,
            (_images <&> (\t -> (t ^. thumbnailId, t))) <> mapSubRs thd3
          )
    j :: ExtractMapsRes -> ExtractMapsRes -> ExtractMapsRes
    j (a, b, c) (a', b', c') = (a <> a', b <> b', c <> c')

mainDebugInfo :: Bool
mainDebugInfo = False

readConfig :: Text -> IO AppConfig
readConfig = toS >>> Y.decodeFileThrow

serverRunner :: ServerRunner
--serverRunner = runServantServer
serverRunner = runYesodServer

main :: IO ()
main = do
  putStrLn $ toS logPrefix <> "Image Hall Back-End by monnef"
  try runMain
    >>=
    --    Left ExitSuccess -> return ()
    \case
      Left (res :: SomeException) -> hPutStrLn stderr $ "ERROR occured: " <> displayException res
      Right _ -> return ()
  where
    runMain :: IO ()
    runMain = do
      let printStrDebug x = when mainDebugInfo $ putStrLn x
      cmdArgs <-
        okWrapper "Parsing command line arguments" $ do
          cmdArgs <- parseCmdArgs
          putStr $ encode cmdArgs & toS
          return cmdArgs
      when (cmdArgs ^. version) $ do
        putStrLn $ toS versionText
        exitSuccess
      config <-
        okWrapper "Processing config" $ do
          config <- readConfig (cmdArgs ^. Data.config)
          putStr $ encode config & toS
          return config
      rawCats <-
        okWrapper "Crawling and processing images" $
          crawl CrawlArgs {config = config, lvl = Public, crawlDebugInfo = False, imageProcessorDebugInfo = False}
      let cats :: [FullCategory] = rawCats <&> partialCategoryToFull
      printStrDebug $ "Cats:\n" <> T.unpack (debugFormatCats cats)
      let (idToCat, idToImage, idToImageThumb) = extractMaps cats & mapTriple (M.fromList, M.fromList, M.fromList)
      printStrDebug $ ppShow idToCat
      let port = config ^. serverPort
      let onBeforeServerStart = printOkMessage
      let hostStr = config ^. serverHost
      let (outHostStr, outPort) = (config ^. serverOutsideHost & T.unpack, config ^. serverOutsidePort)
      let corsOriginMay = bool Nothing (Just $ config ^. corsOrigin) (config ^. corsEnabled)
      let corsStatus :: Text = bool "Off" "On" $ config ^. corsEnabled
      printActionMessage
        [qq|Starting server (listening on $hostStr:$port, outside is $outHostStr:$outPort, CORS: $corsStatus)|]
      initCats <- atomically $ newTVar cats
      initIdToCat <- atomically $ newTVar idToCat
      initIdToImage <- atomically $ newTVar idToImage
      initIdToImageThumb <- atomically $ newTVar idToImageThumb
      let initServerState =
            ServerState
              { _categories = initCats,
                _idToCat = initIdToCat,
                _idToImage = initIdToImage,
                _idToImageThumbnail = initIdToImageThumb
              }
      serverRunner initServerState onBeforeServerStart hostStr port corsOriginMay
