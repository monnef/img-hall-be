{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Crawler
  ( crawl
  , CrawlArgs(..)
  ) where

import           Data
import           ImageProcessor
import           Utils

import           Control.Applicative  ((<|>))
import           Control.Arrow        ((>>>))
import           Control.Lens
import           Control.Monad        (filterM, forM, join, when)
import           Control.Monad.Catch  (MonadThrow)
import           Control.Monad.Except (ExceptT, MonadIO, liftEither, runExceptT)
import           Control.Monad.Trans  (liftIO)
import           Data.Bool            (bool)
import           Data.Function        ((&))
import           Data.Functor         ((<&>))
import qualified Data.HashMap.Lazy    as M
import           Data.List            (intercalate, isPrefixOf, isInfixOf, sort)
import           Data.Maybe           (fromMaybe)
import           Data.Text            (Text)
import qualified Data.Text            as T
import           Safe                 (lastMay)
import           System.Directory     (doesDirectoryExist, doesFileExist,
                                       listDirectory, makeAbsolute)
import           System.FilePath      (splitDirectories, (</>))

type EmptyCategoriesRes = [(PartialCategory, [FilePath])]

emptyCategories :: (MonadIO m, MonadThrow m) => FilePath -> CategoryAccessLevel -> m EmptyCategoriesRes
emptyCategories rootDir lvl = liftIO $ findCategoryPaths rootDir rootDir
  where
    findCategoryPaths :: FilePath -> FilePath -> IO EmptyCategoriesRes
    findCategoryPaths baseDir dir = do
      items <- listDirectory dir <&> fmap (dir </>) <&> sort
      itemDirs <- filterM doesDirectoryExist items <&> filter (not . isInfixOf "/_")
      itemFiles <- filterM doesFileExist items
      subCats <- mapM (findCategoryPaths baseDir) itemDirs <&> concat
      let dirWithoutBase = drop (1 + length baseDir) dir
      let path = T.pack dirWithoutBase
      let label' = dirWithoutBase & splitDirectories & lastMay <&> T.pack & fromMaybe (tshow lvl)
      let label = [(EN, label')]
      let id = path & hashSha512 & CategoryId
      let thisCat =
            PartialCategory
              { _id = id
              , _label = TranslatedText $ M.fromList label
              , _accessLevel = lvl
              , _images = Nothing
              , _path = path
              , _subcategories = subCats
              }
      let r = (thisCat, itemFiles)
      return $ bool [r] [] (null itemFiles && null subCats)

data CrawlArgs = CrawlArgs
  { config                  :: AppConfig
  , lvl                     :: CategoryAccessLevel
  , imageProcessorDebugInfo :: Bool
  , crawlDebugInfo          :: Bool
  }

fillAndGenImages :: (MonadIO m, MonadThrow m) => CrawlArgs -> EmptyCategoriesRes -> m [PartialCategory]
fillAndGenImages CrawlArgs {lvl = catLvl, ..} emptyCats = forM emptyCats processOneCategory
  where
    processOneCategory :: (MonadIO m, MonadThrow m) => (PartialCategory, [FilePath]) -> m PartialCategory
    processOneCategory (cat, imgPaths) = do
      cat' <- liftIO $ processCategoryImages ProcessImageArgs {..} cat imgPaths
      cats <- liftIO $ forM (cat ^. subcategories) $ \p@(_, fs) -> processOneCategory p >>= \r -> return (r, fs)
      return $ (subcategories .~ cats) cat'

crawl :: (MonadIO m, MonadThrow m) => CrawlArgs -> m [PartialCategory]
crawl args@CrawlArgs {..} = do
  when crawlDebugInfo $ liftIO $ putStrLn $ "Data path = '" <> (config ^. dataDir & T.unpack) <> "'"
  let rootDir = rootDirForCatAccessLevel (config ^. dataDir & T.unpack) lvl
  emptyCats <- emptyCategories rootDir lvl
  when crawlDebugInfo $
    liftIO $
    putStrLn $
    emptyCats <&> (\(c, xs) -> (show c <> "\n") <> (xs <&> (show >>> ("  " <>)) & intercalate "\n")) & intercalate "\n"
  fillAndGenImages args emptyCats
