module Utils
  ( tshow,
    mapExtension,
    putStrAndFlush,
    hashSha512,
    mapTriple,
    (>>>),
    (&),
    (<&>),
    toS,
    Text,
  )
where

import Control.Arrow ((>>>))
import qualified Control.Arrow as CA
import qualified Data.ByteString.Lazy as BL
import Data.Digest.Pure.SHA (sha512, showDigest)
import Data.Function ((&))
import Data.Functor ((<&>))
import Data.String.Conv (toS)
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as TLE
import System.FilePath
  ( replaceExtensions,
    takeExtensions,
    (</>),
  )
import System.IO (hFlush, stdout)

tshow :: Show a => a -> Text
tshow = show >>> T.pack

mapExtension :: (String -> String) -> FilePath -> FilePath
mapExtension f x = replaceExtensions x (takeExtensions x & f)

putStrAndFlush :: String -> IO ()
putStrAndFlush x = putStr x >> hFlush stdout

hashSha512 :: Text -> Text
hashSha512 x = x & TL.fromStrict & TLE.encodeUtf8 & sha512 & showDigest & T.pack

mapTriple :: (a -> d, b -> e, c -> f) -> (a, b, c) -> (d, e, f)
mapTriple ~(f, g, h) ~(a, b, c) = (f a, g b, h c)
