{-# LANGUAGE OverloadedStrings #-}

module RunCmd where

import           Utils

import           Control.Monad  (when)
import           Data.Text      (Text)
import qualified Data.Text      as T
import           System.Exit    (ExitCode)
import           System.IO      (BufferMode (NoBuffering), hFlush, hGetContents,
                                 hSetBuffering, hSetEcho, stdout)
import           System.Process (StdStream (CreatePipe), createProcess, cwd,
                                 proc, std_err, std_out, waitForProcess)

runCmd :: Text -> [Text] -> Bool -> Maybe Text -> IO ([Text], [Text], ExitCode)
runCmd cmd args printOutput targetDir = do
--  putStrAndFlush $ T.unpack $ "Running command: " <> T.unwords (cmd : args) <> "\n"
  (_, Just hout, Just herr, jHandle) <-
    createProcess
      (proc (T.unpack cmd) (args <&> T.unpack))
        {cwd = targetDir <&> T.unpack, std_out = CreatePipe, std_err = CreatePipe}
  out <- hGetContents hout
  when printOutput $ putStrLn out
  err <- hGetContents herr
  when printOutput $ putStrLn err
  exitCode <- waitForProcess jHandle
  return (T.lines (out & T.pack), T.lines (err & T.pack), exitCode)
