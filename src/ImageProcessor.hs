{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE ExtendedDefaultRules  #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module ImageProcessor
  ( processCategoryImages
  , ProcessImageArgs(..)
  ) where

import           Data
import           RunCmd
import           Utils

import           Control.Lens
import           Control.Monad                 (forM_, when)
import           Control.Monad.Catch           (Exception, MonadCatch,
                                                MonadThrow, SomeException (..),
                                                displayException, throwM, try)
import           Control.Monad.Extra           (notM, whenM)
import           Control.Monad.Trans           (MonadIO, liftIO)
import           Data.Function                 ((&))
import           Data.Functor                  ((<&>))
import qualified Data.HashMap.Lazy             as M
import           Data.Text                     (Text)
import qualified Data.Text                     as T
import           Data.Time                     (fromGregorian, utctDay)
import qualified Network.URI.Encode            as UE
import           System.Directory              (createDirectoryIfMissing,
                                                doesFileExist,
                                                getModificationTime)
import           System.Exit                   (ExitCode (..))
import           System.FilePath               (takeDirectory, (</>))
import           Text.InterpolatedString.Perl6 (qq)

data ProcessImageArgs = ProcessImageArgs
  { config                  :: AppConfig
  , imageProcessorDebugInfo :: Bool
  }

newtype ImageProcessingException =
  ImageProcessingException Text
  deriving (Show)

instance Exception ImageProcessingException where
  displayException (ImageProcessingException err) = T.unpack err

data ResizeImageArgs = ResizeImageArgs
  { debugInfo             :: Bool
  , srcPath               :: FilePath
  , targetPath            :: FilePath
  , width                 :: Int
  , height                :: Int
  , watermarkPath         :: FilePath
  , watermarkScalePercent :: Int
  }

resizeImage :: (MonadIO m, MonadThrow m, MonadCatch m) => ResizeImageArgs -> m ()
resizeImage ResizeImageArgs {..} = do
  when debugInfo $ liftIO $ putStrLn $ "resizeImage: " <> [qq|$srcPath -> $targetPath; $width x $height|]
  (out, err, exitCode) <-
    do let args =
             [ T.pack srcPath
             , "-dither"
             , "none"
             , "-resize"
             , [qq|{width}x{height}>|]
             , "null:"
             , "("
             , T.pack watermarkPath
             , "-resize"
             , [qq|{watermarkScalePercent}%|]
             , ")"
             , "-gravity"
             , "southeast"
             , "-layers"
             , "composite"
             , "-strip"
             , "-quality"
             , "95"
             , "-colorspace"
             , "sRGB"
             , "-depth"
             , "16"
             , T.pack targetPath
             ]
       liftIO $ runCmd "convert" args False Nothing
  case exitCode of
    ExitSuccess -> return ()
    ExitFailure e -> error $ "Resizing image failed. " <> (err & T.unlines & T.unpack)

processCategoryImages ::
     (MonadIO m, MonadThrow m, MonadCatch m) => ProcessImageArgs -> PartialCategory -> [FilePath] -> m PartialCategory
processCategoryImages ProcessImageArgs {..} cat imgPaths = do
  processedImages <- mapM processImg imgPaths
  return $ (cat :: PartialCategory) {_images = Just processedImages}
  where
    processImg :: (MonadIO m, MonadThrow m, MonadCatch m) => FilePath -> m FullImage
    processImg path = do
      r <- try $ processImgUnsafe path
      case r of
        Left (e :: SomeException) ->
          throwM $ ImageProcessingException $ T.pack path <> ": " <> (displayException e & T.pack)
        Right res -> return res
    processImgUnsafe :: (MonadIO m, MonadThrow m, MonadCatch m) => FilePath -> m FullImage
    processImgUnsafe dataPath = do
      let debugPrint x = when imageProcessorDebugInfo $ liftIO $ print x
      debugPrint ("processImg", dataPath)
      let dropDataDirPrefixT x = T.drop (T.length (config ^. dataDir) + 1) (T.pack x)
      let path = dataPath & dropDataDirPrefixT
      let id = path & hashSha512
      let urlPrefix :: Text = "http://" <> config ^. serverOutsideHost <> ":" <> (config ^. serverOutsidePort & tshow)
      let url = [qq|$urlPrefix/images/{UE.encodeText id}|]
      let thumbnailPath = mapExtension (".thumbnail" <>) (T.unpack path)
      let thumbnailId = thumbnailPath & dropDataDirPrefixT & hashSha512
      let tUrl = [qq|$urlPrefix/images/{UE.encodeText thumbnailId}|]
      date <- getModificationTime dataPath <&> utctDay & liftIO
      let cacheDirT = T.pack (config ^. cacheDir . to T.unpack)
      let cachePath = [qq|$cacheDirT/$path|]
      let cacheThumbPath = [qq|$cacheDirT/$thumbnailPath|]
      debugPrint ("  ", cachePath, cacheThumbPath)
      let [cachePathDir, cacheThumbPathDir] = map takeDirectory [cachePath, cacheThumbPath]
      debugPrint ("  ", cachePathDir, cacheThumbPathDir)
      liftIO $ mapM_ (createDirectoryIfMissing True) [cachePathDir, cacheThumbPathDir]
      let watermarkPath = config ^. watermark & T.unpack
      whenM (liftIO $ notM $ doesFileExist watermarkPath) $ throwM $
        ImageProcessingException [qq|Watermark file "$watermarkPath" not found.|]
      whenM (liftIO $ notM $ doesFileExist cachePath) $
        resizeImage
          ResizeImageArgs
            { debugInfo = imageProcessorDebugInfo
            , srcPath = dataPath
            , targetPath = cachePath
            , width = config ^. targetWidth
            , height = config ^. targetHeight
            , watermarkPath = watermarkPath
            , watermarkScalePercent = 100
            }
      whenM (liftIO $ notM $ doesFileExist cacheThumbPath) $
        resizeImage
          ResizeImageArgs
            { debugInfo = imageProcessorDebugInfo
            , srcPath = dataPath
            , targetPath = cacheThumbPath
            , width = config ^. thumbnailWidth
            , height = config ^. thumbnailHeight
            , watermarkPath = watermarkPath
            , watermarkScalePercent = 20
            }
--      error "TEST"
      return
        FullImage
          { _id = ImageId id
          , _thumbnailId = ImageId thumbnailId
          , _url = ImageUrl url
          , _thumbnailUrl = ImageThumbnailUrl tUrl
          , _date = date
          , _path = cachePath
          , _thumbnailPath = cacheThumbPath
          }
