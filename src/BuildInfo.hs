module BuildInfo where

import Data.Version (showVersion)
import Utils
import Paths_img_hall_be

versionText :: Text
versionText = showVersion version & toS
