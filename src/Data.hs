{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

module Data where

import Control.Concurrent.STM.TVar (TVar)
import Control.Lens hiding ((&))
import Control.Monad.IO.Class (MonadIO)
import Data.Aeson.Encoding (text)
import Data.Aeson.Key
import Data.Aeson.Parser (jstring)
import Data.Aeson.TH hiding (Options)
import Data.Aeson.Types
import Data.Default (Default (..))
import qualified Data.HashMap.Lazy as M
import Data.Hashable (Hashable)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Time (Day)
import GHC.Generics (Generic)
import System.FilePath ((</>))
import Text.Read (readMaybe)
import Utils
import UtilsTH

newtype ImageId
  = ImageId Text
  deriving (Eq, Show, Generic, Hashable)

deriveJSON defaultOptions ''ImageId

newtype ImageUrl
  = ImageUrl Text
  deriving (Eq, Show, Generic)

deriveJSON defaultOptions ''ImageUrl

newtype ImageThumbnailUrl
  = ImageThumbnailUrl Text
  deriving (Eq, Show, Generic)

deriveJSON defaultOptions ''ImageThumbnailUrl

data ImageDetail = ImageDetail
  { _id :: ImageId,
    _thumbnailId :: ImageId,
    _url :: ImageUrl,
    _thumbnailUrl :: ImageThumbnailUrl,
    _date :: Day
  }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''ImageDetail

deriveJSON derJsonOpts ''ImageDetail

data FullImage = FullImage
  { _id :: ImageId,
    _thumbnailId :: ImageId,
    _url :: ImageUrl,
    _thumbnailUrl :: ImageThumbnailUrl,
    _date :: Day,
    _path :: FilePath,
    _thumbnailPath :: FilePath
  }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''FullImage

deriveJSON derJsonOpts ''FullImage

newtype CategoryId
  = CategoryId Text
  deriving (Eq, Show, Generic, Hashable)

deriveJSON defaultOptions ''CategoryId

data Language
  = CS
  | EN
  deriving (Eq, Show, Generic, Hashable, Read)

deriveJSON defaultOptions ''Language

instance ToJSONKey Language where
  toJSONKey = genericToJSONKey defaultJSONKeyOptions

instance FromJSONKey Language where
  fromJSONKey = genericFromJSONKey defaultJSONKeyOptions

newtype TranslatedText
  = TranslatedText (M.HashMap Language Text)
  deriving (Eq, Show, Generic)

deriveJSON defaultOptions ''TranslatedText

unpackTranslatedText (TranslatedText x) = x

data CategoryAccessLevel
  = Public
  | Private
  | Protected
  deriving (Eq, Show, Generic)

deriveJSON defaultOptions ''CategoryAccessLevel

data CategoryInfo = CategoryInfo
  { _id :: CategoryId,
    _label :: TranslatedText,
    _accessLevel :: CategoryAccessLevel,
    _subcategories :: [CategoryInfo]
  }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''CategoryInfo

deriveJSON derJsonOpts ''CategoryInfo

data CategoryDetail = CategoryDetail
  { _id :: CategoryId,
    _label :: TranslatedText,
    _accessLevel :: CategoryAccessLevel,
    _images :: [ImageDetail],
    _subcategories :: [CategoryDetail]
  }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''CategoryDetail

deriveJSON derJsonOpts ''CategoryDetail

data PartialCategory = PartialCategory
  { _id :: CategoryId,
    _label :: TranslatedText,
    _accessLevel :: CategoryAccessLevel,
    _images :: Maybe [FullImage],
    _path :: Text,
    _subcategories :: [(PartialCategory, [FilePath])]
  }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''PartialCategory

deriveJSON derJsonOpts ''PartialCategory

data FullCategory = FullCategory
  { _id :: CategoryId,
    _label :: TranslatedText,
    _accessLevel :: CategoryAccessLevel,
    _images :: [FullImage],
    _path :: Text,
    _detail :: CategoryDetail,
    _info :: CategoryInfo,
    _subcategories :: [FullCategory]
  }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''FullCategory

deriveJSON derJsonOpts ''FullCategory

cAccessLvlToDirName :: CategoryAccessLevel -> Text
cAccessLvlToDirName lvl = lvl & tshow & T.toLower

rootDirForCatAccessLevel :: FilePath -> CategoryAccessLevel -> FilePath
rootDirForCatAccessLevel rawDir lvl = rawDir </> (lvl & cAccessLvlToDirName & T.unpack)

data AppConfig = AppConfig
  { _dataDir :: Text,
    _cacheDir :: Text,
    _watermark :: Text,
    _targetWidth :: Int,
    _targetHeight :: Int,
    _thumbnailWidth :: Int,
    _thumbnailHeight :: Int,
    _serverHost :: Text,
    _serverPort :: Int,
    _serverOutsideHost :: Text,
    _serverOutsidePort :: Int,
    _corsEnabled :: Bool,
    _corsOrigin :: Text
  }

makeFieldsNoPrefix ''AppConfig

deriveJSON derJsonOpts ''AppConfig

instance Default AppConfig where
  def =
    AppConfig
      { _dataDir = "data",
        _cacheDir = "cache",
        _watermark = "watermark.png",
        _targetWidth = 1920,
        _targetHeight = 1080,
        _thumbnailWidth = 400,
        _thumbnailHeight = 400,
        _serverHost = "localhost",
        _serverPort = 8080,
        _serverOutsideHost = "localhost",
        _serverOutsidePort = 8080,
        _corsEnabled = True,
        _corsOrigin = "http://localhost:3000"
      }

data CmdArgs = CmdArgs
  { _generateConfig :: Bool,
    _config :: Text,
    _version :: Bool
  }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''CmdArgs

deriveJSON derJsonOpts ''CmdArgs

type IdToCat = M.HashMap CategoryId FullCategory

type IdToImage = M.HashMap ImageId FullImage

data ServerState = ServerState
  { _categories :: TVar [FullCategory],
    _idToCat :: TVar IdToCat,
    _idToImage :: TVar IdToImage,
    _idToImageThumbnail :: TVar IdToImage
  }
  deriving (Eq, Generic)

makeFieldsNoPrefix ''ServerState

findCat :: Text -> IdToCat -> Maybe FullCategory
findCat catId = M.lookup (CategoryId catId)

type ServerRunner = ServerState -> IO () -> Text -> Int -> Maybe Text -> IO ()
