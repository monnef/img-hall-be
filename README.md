# img-hall-be

# Installation via Docker

```sh
docker run -d --rm \
  --name img-hall-be \
  -v "$(pwd)"/data:/app/data:ro \
  -v "$(pwd)"/watermark.png:/app/watermark.png:ro \
  -v "$(pwd)"/cache:/app/cache \
  -v "$(pwd)"/config.toml:/app/config.toml \
  --net=host \
  mon7/img-hall-be
```

The command above is expected to be run from a directory where the [setup](#setup) was done.

# Manual installation

## Requirements

* [stack](http://haskellstack.org)

## Building

```sh
stack build
```

## Running

```sh
stack exec img-hall-be-exe
```

# Setup

## Data

Copy your images to `data` directory.

## Configuration

```sh
cp config.sample.toml config.toml
vim config.toml
```
